$jq10 = jQuery.noConflict();
$jq10(document).ready(function () {

    try {
        if ($jq10('#marquee-component').length) {
            startSlider("mslider");
        }
    } catch (err) {
        //throw err.message;
    }
    try {
        if ($jq10('#hero-component').length && $jq10('#hero-component').has('li').length > 0) {
            startSlider("rslides");
        }
    } catch (err) {
        //throw err.message;
    }

    if ((document.getElementById('ie') == null) && (document.getElementById('search_wholesale') == null)) {
        if ((window.innerWidth || document.documentElement.clientWidth) <= 1024) {
            searchAsYouType.initialize(document.getElementById('qm'), false, true);
        } else {
            searchAsYouType.initialize(document.getElementById('q'), false, true);
        }
    }
    //removes menu divider on load from active-page id
    $jq10("#primaryNav").find('#active-page').addClass('no-menu-divider');

    // add has-submenu class to the parent li where submenu is available
    $jq10('.wholesale-menu').find('li:has(.secondaryNav)').addClass('has-submenu');

    $jq10("#primaryNav li").mouseover(function () {
        $jq10(this).parent('ul').find('#active-page').removeClass('no-menu-divider');
        $jq10(this).parent('ul').find('#active-page').css('background', '#131313');
        $jq10(this).parent('ul').find('#active-page div.menu-divider a:first-child').css('color', '#fff');
    });
    $jq10("#primaryNav li").mouseout(function () {
        $jq10(this).parent('ul').find('#active-page').addClass('no-menu-divider');
        $jq10(this).parent('ul').find('#active-page').css('background', '#fff');
        $jq10(this).parent('ul').find('#active-page div.menu-divider a:first-child').css('color', '#067AB4');
    });
    $jq10('li#active-page').mouseover(function () {
        $jq10(this).parent('ul').find('#active-page').removeClass('no-menu-divider');
        $jq10(this).parent('ul').find('#active-page').css('background', '#fff');
        $jq10(this).parent('ul').find('#active-page div.menu-divider a:first-child').css('color', '#067AB4');
    });

    $jq10("#icon-menu").click(function () {
        if ($jq10("#icon-menu").hasClass('menu-icon')) {
            if (($jq10(".header-wrapper").hasClass("sticky")) && (!$jq10("#side-wrapper-header").hasClass("sticky"))) {
                $jq10("#side-wrapper-header").addClass("sticky");
            }
            $jq10("#side_wrapper").show(function () {
                $jq10("#wrapper").css({ "moz-box-shadow": "0 0 8px #666", "webkit-box-shadow": "0 0 8px #666", "box-shadow": "0 0 8px #666", "float": "left" });
                $jq10("#ie #wrapper").css({ "border-right": "solid 1px #666" });
                $jq10("#icon-menu").removeClass('menu-icon');
                $jq10("#icon-menu").addClass('close-icon');
                $jq10('#wrapper').css({ "margin": "0 0 0 -285px" });
                $jq10("#ie #wrapper").css({ "margin-left": "-281" });
                if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                    //$jq10("#header").css("float","right");
                    //$jq10("#header").css("max-width","200px");
                    //$jq10("#primary-wrapper").css("max-width","480px");
                } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                    //$jq10("#header").css("float","right");
                    //$jq10("#header").css("max-width","488px");
                    //$jq10("#primary-wrapper").css("max-width","768px");
                }
                //$jq10("#contact-button").hide();
                //$jq10("#share-button").hide();
                $jq10(".widget-button-holder").hide();
                $jq10('html, body').animate({
                    scrollTop: $jq10("#primary-wrapper").offset().top
                });
            });
            ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'SubNav', 'contentFriendlyName': 'Primary Navigation', 'linkName': 'Menu Icon', 'linkDestinationUrl': 'Open Mobile Nav' });
            if (displayAlerts == "True") alert("Tracking Testing");
        } else {
            hideMenu();
            ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'SubNav', 'contentFriendlyName': 'Primary Navigation', 'linkName': 'Close Menu Icon', 'linkDestinationUrl': 'Close Mobile Nav' });
            if (displayAlerts == "True") alert("Tracking Testing");
        }
    });

    $jq10(".arrow_prod_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.product > li").addClass("active_sub_menu_items");
        $jq10(".product").show();
        $jq10(".arrow_prod_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_prod_m").css("display", "none");
        $jq10(".sub_back_arrow").css("display", "none");
    });
    $jq10(".arrow_goto_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.goto > li").addClass("active_sub_menu_items");
        $jq10(".goto").css("display", "block");
        $jq10(".arrow_goto_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_goto_m").css("display", "none");
    });
    $jq10(".arrow_personal_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.personal > li").addClass("active_sub_menu_items");
        $jq10(".personal").css("display", "block");
        $jq10(".arrow_personal_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_personal_m").css("display", "none");
    });
    $jq10(".arrow_business_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.business > li").addClass("active_sub_menu_items");
        $jq10(".business").css("display", "block");
        $jq10(".arrow_business_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_business_m").css("display", "none");
    });
    $jq10(".arrow_about_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.about > li").addClass("active_sub_menu_items");
        $jq10(".about").css("display", "block");
        $jq10(".arrow_about_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_about_m").css("display", "none");
    });
    $jq10(".arrow_account_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.account > li").addClass("active_sub_menu_items");
        $jq10(".account").css("display", "block");
        $jq10(".arrow_account_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_account_m").css("display", "none");
    });
    $jq10(".arrow_blog_m").click(function () {
        $jq10(this).parent("li").addClass("activemenu");
        $jq10("ul.blog > li").addClass("active_sub_menu_items");
        $jq10(".blog").css("display", "block");
        $jq10(".arrow_blog_m").parent("li").siblings().css("display", "none");
        $jq10(".back_arrow").css("display", "block");
        $jq10(".arrow_blog_m").css("display", "none");
    });
    $jq10(".back_arrow").click(function () {
        $jq10(this).parent("li").removeClass("activemenu");
        $jq10(".active_sub_menu_items").removeClass("active_sub_menu_items");
        $jq10(".arrow_prod_m,.arrow_account_m,.arrow_blog_m,.arrow_goto_m,.arrow_personal_m,.arrow_business_m,.arrow_about_m").css("display", "block");
        $jq10(".arrow_prod_m,.arrow_account_m,.arrow_blog_m,.arrow_goto_m,.arrow_personal_m,.arrow_business_m,.arrow_about_m").parent("li").siblings().css("display", "block");
        $jq10(".sub_menu_wrap,.back_arrow").css("display", "none");
    });

    $jq10(".sub_arrow_m").click(function () {
        $jq10(".activemenu,.sub_arrow_m").css("display", "none");
        $jq10(".active_sub_menu_items").removeClass("active_sub_menu_items");
        $jq10(this).parent("li").addClass("active_sub_menu");
        $jq10(this).parent("li").siblings().hide();
        $jq10("li.active_sub_menu > ul").addClass("active_sub_menu_wrapper");
        $jq10("ul.active_sub_menu_wrapper > li").addClass("active_sub_menu_items");
        $jq10(".sub_back_arrow").css("display", "block");
    });

    $jq10(".sub_back_arrow").click(function () {
        $jq10(".activemenu,.sub_arrow_m").css("display", "block");
        $jq10(".active_sub_menu_items").removeClass("active_sub_menu_items");
        $jq10(".active_sub_menu_wrapper").removeClass("active_sub_menu_wrapper");
        $jq10(this).parent("li").removeClass("active_sub_menu").addClass("active_sub_menu_items");
        $jq10(this).parent("li").siblings().show().addClass("active_sub_menu_items");
        $jq10(".sub_back_arrow").css("display", "none");
    });

    $jq10(window).resize(function () {
        if ((window.innerWidth || document.documentElement.clientWidth) >= 1024) {
            hideMenu();
        } else {
            if ($jq10("#icon-menu").hasClass('menu-icon')) {
                if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                    //$jq10("#header").css("max-width","480px");
                } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                    //$jq10("#header.resp-header").css("max-width","768px");
                } else {
                    $jq10("#header.resp-header").css("max-width", "980px");
                    $jq10("#header.fluid-header").css("max-width", "1280px");
                    $jq10("#header").css("float", "inherit");

                }
            } else if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
                //$jq10("#header").css("max-width","200px");
                //$jq10("#header").css("float","right");
                //$jq10("#primary-wrapper").css("max-width","480px");
            } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
                //$jq10("#header").css("max-width","488px");
                //$jq10("#header").css("float","right");
                //$jq10("#primary-wrapper").css("max-width","768px");
            }
        }
        if ((window.innerWidth || document.documentElement.clientWidth) <= 479) {
            $jq10("#formFrame .bottom-form").css("height", "650px");
        } else if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
            $jq10("#formFrame .bottom-form").css("height", "425px");
        } else {
            $jq10("#formFrame .bottom-form").css("height", "375px");
        }
        var resizeTimer = setTimeout(function () {
            padPercent = ($jq10("#child-copy2-overlay").height() / ($jq10("#child-copy2-overlay").width())) * 100;
            $jq10(".child-copy-overlay .image-block").css("padding-top", padPercent + '%');
        }, 200);
    });

    if ($jq10.cookie("toggleaddinfo") == "y") {
        toggleAddInfo();
    }

    //Smooth scrolling to onpage ID code
    $jq10('a[href*=#]:not([href=#])').click(function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $jq10(this.hash);
            target = target.length ? target : $jq10('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                var targetOffset = target.offset().top;
                if ($jq10(".module-page-nav").hasClass("sticky")) {
                    targetOffset = targetOffset - 95;
                } else if ($jq10(".header-wrapper").hasClass("sticky")) {
                    targetOffset = targetOffset - 55;
                } else if ($jq10(".module-page-nav").length) {
                    targetOffset = targetOffset - 240;
                } else {
                    targetOffset = targetOffset - 110;
                }
                $jq10('html,body').animate({
                    scrollTop: targetOffset
                }, 1000);
                e.preventDefault();
                return false;
            }
        }
    });

    //contact button widget on click
    $jq10(function () {
        var isMobile;
        var bottomFormLen = 0;
        $jq10("#contact-button").click(function () {
            //setting the height of 'html' because in formFrameAdjustHt() it checks the height of this element
            //$jq10('#formFrame').contents().find('html').css("height","518px");
            //check if thank you message is being shown
            checkIfMobile();
            dcsMultiTrack('DCSext.wtATTPage', 'prod_serv', 'DCSext.wtATTAtrib1', 'contact-button');
            ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'contact-button', 'contentFriendlyName': 'Contact Button', 'linkName': 'Contact Button', 'linkDestinationUrl': 'Contact Form' });
        });

        $jq10(window).resize(function () {
            if ($jq10("#formFrame.right-rail").is(':visible')) {
                checkIfMobile();
            }
            formFrameAdjustHt('first', 1);
        });

        function checkIfMobile() {
            //isMobile=window.innerWidth<768;
            if ((window.innerWidth || document.documentElement.clientWidth) < 768) { isMobile = true; } else { isMobile = false; }
            if (isMobile) {
                if (bottomFormLen == 0)
                    bottomFormLen = $jq10("#bottom-form").offset().top;
                $jq10('html, body').animate({
                    scrollTop: bottomFormLen
                }, function () { formFrameAdjustHt('first', 1); });
            } else {
                setContactModal();
            }
            if ($jq10("#formFrame").contents().find('#mainwindow').length > 0) {
                $jq10("#formFrame").attr("src", $jqform("#iotLeadFormUrl").val());
                //$jq10('#formFrame').contents().find('html').css("height","518px");              
            }
        }

        function setContactModal() {

            var timer,
                interval = 500,
                timeout = 3000,  // 3 seconds
                timeSpent = 0,
                classes = {
                    bottomForm: 'bottom-form',
                    rightRail: 'right-rail'
                },
                $formFrame = $jq10('#formFrame'),
                $formFrameContents = $formFrame.contents();

            $formFrame.css({
                display: 'inline',
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
                margin: 'auto'
            });

            if (isMobile) {
                $formFrame.css({
                    height: '100%',
                    width: '100%'
                });

            } else {
                $formFrame.css({
                    height: '518px',
                    width: '80%',
                    maxWidth: '620px'
                });
            }
            var test = $formFrame.css('height');

            // poll until iframe loads
            if ($formFrame.contents().find('body').length > 0 && $formFrame.contents().find('#form-header').length > 0) {
                $formFrame.contents().find('body')
                    .removeClass(classes.bottomForm)
                    .addClass(classes.rightRail);
            }

            $formFrame
                .removeClass(classes.bottomForm)
                .addClass(classes.rightRail);

            formFrameAdjustHt('first', 1);
            makeButtonActive("contact-button");
            maskWidgetsModal();
        }

    });


    //click of share widget button
    $jq10("#share-button").click(function () {
        $jq10("#social-component").removeClass("social-bottom").addClass("social-right-rail");
        maskWidgetsModal();
        makeButtonActive("share-button");
        dcsMultiTrack('DCSext.wtATTPage', 'prod_serv', 'DCSext.wtATTAtrib1', 'share-button');
        ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'share-button', 'contentFriendlyName': 'Share Button', 'linkName': 'Share Button', 'linkDestinationUrl': 'Share Panel' });
    });
    //click of close button in share widget modal
    $jq10(".social-close").click(function () {
        $jq10("#social-component").removeClass("social-right-rail").addClass("social-bottom");
        unmaskWidgetsModal();
        makeButtonInactive("share-button");
    });



    //foresee overlay modal
    $jq10("#foresee-button").click(function (e) {
        e.preventDefault();
        maskWidgetsModal();
        $jq10("#foresee-modal").show();
        makeButtonActive("foresee-button");
    });

    $jq10(".foresee-modal-close").click(function () {
        unmaskWidgetsModal();
        $jq10("#foresee-modal").hide();
        makeButtonInactive("foresee-button");
    });

    function makeButtonActive(el) {
        var elm = $jq10("#" + el);
        if (!elm.hasClass('active')) {
            elm.addClass('active');
        }
    }
    function makeButtonInactive(el) {
        var elm = $jq10("#" + el);
        if (elm.hasClass('active')) {
            elm.removeClass('active');
        }
    }

    // masks the page when widget modal appears
    function maskWidgetsModal() {
        if (!$jq10('.body').hasClass('noScroll')) {
            $jq10(".body").addClass('noScroll');
        }
        if ($jq10(".body").find('#overlay-widgets-mask').length == 0) {
            $jq10(".body").append('<div id="overlay-widgets-mask"></div>');
        }

    }
    // unmasks the page when widget modal disappears
    function unmaskWidgetsModal() {
        if ($jq10('.body').hasClass('noScroll')) {
            $jq10(".body").removeClass('noScroll');
        }
        if ($jq10(".body").find('#overlay-widgets-mask').length > 0) {
            $jq10('#overlay-widgets-mask').remove();
        }

    }

    //making the widgets invisible when any of the following elemets are visible    
    $jq10(function () {
        console.log("Got in");
        var isVisibleForesee, isVisibleBottomForm, isVisibleSocialComp;
        if ($jq10('#foresee-component').length) {
            $jq10('#foresee-component').bind('inview', function (e, isVisible, topOrBottomOrAll) {
                isVisibleForesee = isVisible;
                checkifInview();
            });
        }
        if ($jq10('#bottom-form').length) {
            $jq10('#bottom-form').bind('inview', function (e, isVisible, topOrBottomOrAll) {
                isVisibleBottomForm = isVisible;
                checkifInview();
            });
        }
        if ($jq10('#shareInviewContainer').length) {
            $jq10('#shareInviewContainer').bind('inview', function (e, isVisible, topOrBottomOrAll) {
                isVisibleSocialComp = isVisible;
                checkifInview();
            });
        }

        function checkifInview() {
            if (!isVisibleForesee && !isVisibleBottomForm && !isVisibleSocialComp) {
                showWidgetButtons();
            }
            if (isVisibleForesee || isVisibleBottomForm || isVisibleSocialComp) {
                hideWidgetButtons();
            }
        }

        function hideWidgetButtons() {
            $jq10(".widget-button-holder").hide();
            $jq10("#widget-buttons-sh").hide();
            $jq10("#formFrame").removeClass("right-rail");
            $jq10("#formFrame").addClass("bottom-form");
            $jq10("#formFrame").css("position", "relative");
            $jq10("#formFrame").css("display", "block");
            $jq10("#formFrame").css("width", "100%");
            $jq10("#formFrame").css("max-width", "980px");
            $jq10("#formFrame").css("margin", "0px auto");
            $jq10("#formFrame").css("top", "0px");
            $jq10("#formFrame").css("border", "0px");
            formFrameAdjustHt("first", 1);
            unmaskWidgetsModal();
            $jq10("#foresee-modal").hide();
            if ($jq10("#social-component").hasClass('social-right-rail'))
                $jq10("#social-component").removeClass("social-right-rail").addClass("social-bottom");
            $jq10("#formFrame").contents().find('body').removeClass("right-rail");
            $jq10("#formFrame").contents().find('body').addClass("bottom-form");
            $jq10("#formFrame").contents().find('#comments-div').css("clear", "both");
            if ($jq10("#formFrame").contents().find('#mainwindow').length) {
                $jq10("#bottom-form").css("min-height", "175px");
                $jq10("#formFrame").contents().find('body').css("background-color", "#067ab4");
                $jq10("#formFrame").css("height", "175px");
            }
            //$jq10("#social-component").removeClass("social-right-rail");
            //$jq10("#social-component").addClass("social-bottom");
        }
        function showWidgetButtons() {
            $jq10(".widget-button-holder").show();
            $jq10("#widget-buttons-sh").show();
            $jq10("#formFrame").hide();
            $jq10("#formFrame").removeClass("bottom-form");
            $jq10("#formFrame").addClass("right-rail");
            $jq10("#formFrame").contents().find('body').removeClass("bottom-form");
            $jq10("#formFrame").contents().find('body').addClass("right-rail");
            $jq10("#formFrame").contents().find('#comments-div').css("clear", "none");
            $jq10("#formFrame").css("position", "fixed");
            $jq10("#formFrame").css("width", "300px");
            $jq10("#formFrame").css("right", "0px");
            $jq10("#formFrame").css("top", "100px");
            $jq10("#formFrame").css("height", "500px");
            unmaskWidgetsModal();
            if ($jq10("#formFrame").css("display") != "none") {
                formFrameAdjustHt("first", 1);
            }
            if ($jq10("#formFrame").contents().find('#mainwindow').length) {
                $jq10("#formFrame").css("height", "175px");
                $jq10("#formFrame").contents().find('body').css("background-color", "#e8e8e8");
            }
        }

    });






    // Sticky Primary NAV
    if ($jq10('#bottom-form').length || $jq10('#new-footer').length) {
        var stickyNavTop = $jq10('.header-wrapper').offset().top;
        var stickyNavBottom;
        if ($jq10('#bottom-form').length) {
            stickyNavBottom = $jq10('#bottom-form').offset().top - 110;
        }

        function stickyNav() {
            var scrollTop = $jq10(window).scrollTop();
            var navBottom = ($jq10('#bottom-form').length) ? (scrollTop < stickyNavBottom) : true;
            if ((scrollTop > stickyNavTop) && navBottom) {
                $jq10('.header-wrapper').addClass('sticky');
                $jq10('#side-wrapper-header').addClass('sticky');
                $jq10('.module-page-nav').addClass('sticky');
            } else {
                $jq10('.header-wrapper').removeClass('sticky');
                $jq10('#side-wrapper-header').removeClass('sticky');
                $jq10('.module-page-nav').removeClass('sticky');
            }
        }
        var doScrollCheck = "true";
        $jq10(window).scroll(function () {
            if (doScrollCheck === "true") { stickyNav(); }
        });
    }

    // onpage video
    if ($jq10('.onpage-video').length) {
        $jq10('.onpage-video').each(function (index) {
            loadPlayer($jq10(this).attr('id'), $jq10(this).attr('data-width'), $jq10(this).attr('data-height'), $jq10(this).attr('data-image'), $jq10(this).attr('data-path'), $jq10(this).attr('data-cc-file'), 'html5', 'false', 'true');
        });
    }
});

//Adobe Tracking Scripts
function getUIExperience() {
    var uiExperience = "";
    if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
        uiExperience = "Smartphone";
    } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
        uiExperience = "Tablet";
    } else if ((window.innerWidth || document.documentElement.clientWidth) >= 1024) {
        uiExperience = "Desktop";
    }
    return uiExperience;
}

function formFrameAdjustHt(status, options) {
    // if senario is not passed to make sure default case executes
    var scenario = options || 999,
        $formFrame = $jq10('#formFrame.bottom-form');
    switch (scenario) {
        // desktop comment is open
        case 1:
            if (status == "first") {
                // if((window.innerWidth || document.documentElement.clientWidth) < 768) {
                // $formFrame.css('height', '100%');
                //} else {
                //$formFrame.css('height', $formFrame.contents().find('#form-header').innerHeight());
                //$formFrame.css('height', $formFrame.contents().find('#formHeightCalc').innerHeight());
                $formFrame.css('height', $formFrame.contents().find('.widget-header').innerHeight() + $formFrame.contents().find('.widget-content').innerHeight() + 20);
                //}
            }
            else if (status == "open") {
                $formFrame.css('height', $formFrame.height() + 90 + 'px');
            }
            else if (status == "close") {
                $formFrame.css('height', $formFrame.height() - 90 + 'px');
            }

            break;
        default:
            var paddinght = (status == "textarea") ? 30 : 0;
            var framehtmlHt = $jq10('#formFrame').contents().find('html').height() + paddinght;
            $jq10('#formFrame').css("height", framehtmlHt + "px");

            if ($jq10('#formFrame').css("position") == "fixed") {
                var fheight = $jq10('#formFrame').height();
                if (status == "true" || fheight > 550) {
                    $jq10('#formFrame').css("top", ((fheight > 660) ? 0 : (660 - fheight)) + "px");
                    $jq10('#formFrame').css("z-index", "1001");
                }
                $jq10('#formFrame').contents().find('#commentsfield').css("max-height", "150px");
            } else {
                //$jq10('#bottom-form').css("height", framehtmlHt + "px");
            }
            break;
    }

}

function leadformConf() {
    var bottomFormLen;
    var isMobile = ((window.innerWidth || document.documentElement.clientWidth) < 768) ? true : false;
    if (isMobile) {
        bottomFormLen = $jq10("#bottom-form").offset().top;
        $jq10('html, body').animate({
            scrollTop: bottomFormLen
        }, function () { formFrameAdjustHt('first', 1); });
    }
    if ($jq10('#formFrame').css("position") == "relative") {
        $jq10("#formFrame").contents().find('body').removeClass("right-rail").addClass("bottom-form");
        $jq10('#formFrame').css("height", "175px");
        $jq10('#bottom-form').css("min-height", "175px");
    } else if ($jq10('#formFrame').css("position") == "fixed") {
        $jq10("#formFrame").contents().find('body').removeClass("bottom-form").addClass("right-rail");
        $jq10('#formFrame').css("height", "205px");
        $jq10('#bottom-form').css("min-height", "400px");
    }
}

//function call is within the form page; on load of the page to update the class of body to the same as iFrame
function updateBodyClass() {
    console.log('updateBodyClass called');
    if ($jq10('#formFrame').hasClass('right-rail')) {
        $jq10('#formFrame').contents().find('body').removeClass('bottom-form').addClass('right-rail');

    } else if ($jq10('#formFrame').hasClass('bottom-form')) {
        $jq10('#formFrame').contents().find('body').removeClass('right-rail').addClass('bottom-form');
    }
}

function setActiveMenuStyles() {
    $jq10(this).parent("li").addClass("activemenu");
    if ($jq10.browser.msie) {
        if ($jq10.browser.version <= 8) {
            // Set activemenu styles
            $jq10(this).parent("li").animate({
                padding: '20px 0 0',
                borderTopWidth: '0'
            });
        }
    }
}

function setActiveSubmenuStyles(elementToUpdate) {
    $jq10(elementToUpdate).addClass("active_sub_menu_items");
    if ($jq10.browser.msie) {
        if ($jq10.browser.version <= 8) {
            // Set active_sub_menu_items styles
            $jq10("elementToUpdate").animate({
                padding: '20px 0 17px',
                margin: '0 !important',
                borderTopWidth: '0',
            });
            $jq10(elementToUpdate).css("background-color", "#FFF !important");
        }
    }
}

function hideMenu() {
    $jq10("#side_wrapper").hide(function () {
        $jq10("#wrapper").css({ "moz-box-shadow": "0 0 0", "webkit-box-shadow": "0 0 0", "box-shadow": "0 0 0", "float": "none" })
        $jq10("#ie #wrapper").css({ "border-right": "none" });
        $jq10("#header .logo").css("display", "block");
        $jq10("#header .segment").css("display", "block");
        if ($jq10("#icon-menu").hasClass('close-icon')) {
            $jq10("#icon-menu").removeClass('close-icon');
            $jq10("#icon-menu").addClass('menu-icon');
        }
        $jq10('#wrapper').css({ "margin": "0px auto" });
        $jq10('#primary-wrapper').css({ "max-width": "100%" });
        if ((window.innerWidth || document.documentElement.clientWidth) <= 767) {
            //$jq10("#header").css("max-width","480px");
        } else if ((window.innerWidth || document.documentElement.clientWidth) <= 1023) {
            //$jq10("#header.resp-header").css("max-width","768px");
        } else {
            $jq10("#header.resp-header").css("max-width", "980px");
            $jq10("#header.fluid-header").css("max-width", "1280px");
        }
        //$jq10("#header").css("float","none")
        //$jq10("#contact-button").show();
        //if ((window.innerWidth || document.documentElement.clientWidth) > 767) {
        //$jq10("#share-button").show();
        //}
        $jq10(".widget-button-holder").show();
    });
}

function toggleAddInfo() {
    if ($jq10("#add-info-image").hasClass('grey-carat-up-icon')) {
        $jq10.cookie("toggleaddinfo", "n");
        $jq10("#add-info-image").removeClass('grey-carat-up-icon');
        $jq10("#add-info-image").addClass('grey-carat-down-icon');
        document.getElementById('add-info-component').style.display = 'none';
        ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'learn-more', 'contentFriendlyName': $jq10(this).text(), 'linkName': $jq10(this).text(), 'linkDestinationUrl': 'Collapse' });
    } else {
        $jq10.cookie("toggleaddinfo", "y");
        $jq10("#add-info-image").removeClass('grey-carat-down-icon');
        $jq10("#add-info-image").addClass('grey-carat-up-icon');
        document.getElementById('add-info-component').style.display = 'block';
        ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'learn-more', 'contentFriendlyName': $jq10(this).text(), 'linkName': $jq10(this).text(), 'linkDestinationUrl': 'Expand' });
    }
    return (false);
}

function startSlider(sliderItem) {
    $jq10("." + sliderItem).responsiveSlides({
        namespace: sliderItem,
        auto: true,
        pager: true,
        nav: true,
        speed: 1500,
        timeout: 7000,
        pause: true
    });
}

jQuery.cookie = function (name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        var path = options.path ? '; path=' + options.path : '';
        var domain = options.domain ? '; domain=' + options.domain : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};

if ($jq10('#tab-component').length) {
    $jq10.getScript('/library/javascript/resp-tabs.js', function () {
        $jq10('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion 
            width: 'auto', //auto or any width like 600px
            fit: true // 100% fit in a container
        });
        $jq10('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });

        $jq10('ul.resp-tabs-list li').click(function (e) {
            $jq10.cookie("resptabsel", $jq10(this).find("a").attr("id"));
            var textV = $jq10(this).find("a").text();
            var positionli = $jq10(this).parent().children().index(this) + 1;
            var hashV = "#tab" + positionli;
            if (textV == "Resource Library" || textV == "ResourceLibrary") hashV = "#resource";
            if (window.location.hash == hashV) { return false; }
            window.location.hash = hashV;
            var textAtlas = $jq10(this).find("a").attr("data-atlasCode");
            dcsMultiTrack('DCS.dcsuri', window.location.pathname, 'DCS.dcsref', window.location.href, 'DCSext.wtPN', textV + ' Tab', 'WT.ti', document.title + ': ' + textV + ' Tab');
            trackAtlasTab(textAtlas);
            ddo.pushEvent('linkClick', 'Link_Click', { 'slotFriendlyName': 'tab-component', 'contentFriendlyName': textV, 'linkName': textV, 'linkDestinationUrl': hashV });
        });

        var resourcesel = $jq10.cookie("resourcesel");
        if ((resourcesel == "") || (resourcesel == "null") || (resourcesel == null) || (resourcesel == "all-resources")) {
            $jq10('#resource-list option:first-child').attr("selected", "selected");
        } else {
            $jq10('#resource-list option[value="' + resourcesel + '"]').attr("selected", "selected");
            $jq10(".all_resources_divider,.resource-item,.resource-list-header,.m_all_resources,.all_resources_divider").hide();
            $jq10('#' + resourcesel).show();
            $jq10(".m_recent_first,.resources_divider").show();
            $jq10('#resource-list-selected-header').text($jq10('#resource-list option:selected').text());
            $jq10(".resource-item").css("margin-bottom", "0px");
        }
    });

}

function loadPlayer(pDivId, pWidth, pHeight, pImage, pURL, pSRT, pPrimary, pAutoStart, pSharingEnabled) {
    var skinFile = "/library/media/player/skins/att/video.xml";
    var playerHeight = pHeight;
    var playerWidth = pWidth;
    var currentDuration = null;
    var currentTime = null;
    var isTenSeconds = false;
    var isMidPoint = false;
    var isStartVideo = false;
    var videoLengthTotal = null;
    var isQuarter = false;
    var isMilestone3 = false;
    var autoStart = "1";

    if (pAutoStart === "false") autoStart = "0";

    if (pSharingEnabled) {
        jwplayer(pDivId).setup({
            playlist: [{
                image: pImage,
                file: pURL,
                tracks: [{
                    file: pSRT,
                    label: "English",
                    kind: "captions",
                    "default": true
                }]
            }],
            sharing: {
                code: encodeURI(window.location.href),
                link: encodeURI(window.location.href)
            },
            id: pDivId,
            name: pDivId,
            height: playerHeight,
            width: playerWidth,
            skin: skinFile,
            autostart: pAutoStart,
            primary: pPrimary
        });
    } else {
        jwplayer(pDivId).setup({
            playlist: [{
                image: pImage,
                file: pURL,
                tracks: [{
                    file: pSRT,
                    label: "English",
                    kind: "captions",
                    "default": true
                }]
            }],
            id: pDivId,
            name: pDivId,
            height: playerHeight,
            width: playerWidth,
            skin: skinFile,
            autostart: pAutoStart,
            primary: pPrimary
        });
    }

    jwplayer(pDivId).onPlay(function (event) {
        if (!isStartVideo) {
            isStartVideo = true;
            dcsMultiTrack('DCSext.wtATTPage', 'prod_serv', 'DCSext.wtATTAtrib1', 'featured-video', 'DCSext.wtATTAtrib2', 'video', 'DCSext.wtATTAtrib3', pDivId);
            var player = this;
            player.durationCheck = setInterval(function () {
                if (player.getDuration() !== -1) {
                    videoLengthTotal = Math.round(player.getDuration());
                    videoLengthTotal = getMinutesSeconds(videoLengthTotal);
                    ddo.pushEvent('video', 'MCOM_Video_Start', { 'successFlag': '1', 'linkName': pDivId, 'videoPlayerName': 'jwPlayer', 'videoFriendlyName': pDivId, 'fileName': pDivId, 'videoAutoPlayFlag': autoStart, 'linkDestinationUrl': pURL, 'videoLocation': 'Body', 'videoLengthTotal': videoLengthTotal });
                    clearTimeout(player.durationCheck);
                }
            }, 250);
        }
    });
    jwplayer(pDivId).onComplete(function (event) {
        flashTrack('end_video');
        ddo.pushEvent('video', 'MCOM_Video_Complete', { 'successFlag': '1', 'linkName': pDivId, 'videoPlayerName': 'jwPlayer', 'videoFriendlyName': pDivId, 'fileName': pDivId, 'videoAutoPlayFlag': autoStart, 'linkDestinationUrl': pURL, 'videoLocation': 'Body', 'videoLengthTotal': videoLengthTotal, 'videoLengthViewed': videoLengthTotal, 'videoWatchedPercent': '100%', 'videoProgressionPercent': '100%', 'videoProgressionType': 'Milestone' });
    });
    jwplayer(pDivId).onFullscreen(function (event) {
        if (event.fullscreen) {
            flashTrack('full_screen');
        }
    });
    jwplayer(pDivId).onTime(function (event) {
        currentDuration = Math.round(event.duration * 10) / 10;
        currentTime = event.position;
        var currentLengthViewed = getMinutesSeconds(currentTime);
        var milestone1 = Math.round(currentDuration / 4);
        //var midpoint = Math.round(currentDuration/2) ;
        var midpoint = Math.round(milestone1 * 2);
        var milestone3 = Math.round(milestone1 * 3);
        if ((currentTime >= 10.0 && currentTime < midpoint) && !isTenSeconds) {
            isTenSeconds = true;
            flashTrack('ten_seconds');
        } else if ((currentTime >= milestone1 && currentTime < currentDuration) && !isQuarter) {
            isQuarter = true;
            ddo.pushEvent('video', 'MCOM_Video_Progress', { 'successFlag': '1', 'linkName': pDivId, 'videoPlayerName': 'jwPlayer', 'videoFriendlyName': pDivId, 'fileName': pDivId, 'videoAutoPlayFlag': autoStart, 'linkDestinationUrl': pURL, 'videoLocation': 'Body', 'videoLengthTotal': videoLengthTotal, 'videoLengthViewed': currentLengthViewed, 'videoWatchedPercent': '25%', 'videoProgressionPercent': '25%', 'videoProgressionType': 'Milestone' });
        } else if ((currentTime >= midpoint && currentTime < currentDuration) && !isMidPoint) {
            isMidPoint = true;
            flashTrack('midpoint');
            ddo.pushEvent('video', 'MCOM_Video_Progress', { 'successFlag': '1', 'linkName': pDivId, 'videoPlayerName': 'jwPlayer', 'videoFriendlyName': pDivId, 'fileName': pDivId, 'videoAutoPlayFlag': autoStart, 'linkDestinationUrl': pURL, 'videoLocation': 'Body', 'videoLengthTotal': videoLengthTotal, 'videoLengthViewed': currentLengthViewed, 'videoWatchedPercent': '50%', 'videoProgressionPercent': '50%', 'videoProgressionType': 'Milestone' });
        } else if ((currentTime >= milestone3 && currentTime < currentDuration) && !isMilestone3) {
            isMilestone3 = true;
            ddo.pushEvent('video', 'MCOM_Video_Progress', { 'successFlag': '1', 'linkName': pDivId, 'videoPlayerName': 'jwPlayer', 'videoFriendlyName': pDivId, 'fileName': pDivId, 'videoAutoPlayFlag': autoStart, 'linkDestinationUrl': pURL, 'videoLocation': 'Body', 'videoLengthTotal': videoLengthTotal, 'videoLengthViewed': currentLengthViewed, 'videoWatchedPercent': '75%', 'videoProgressionPercent': '75%', 'videoProgressionType': 'Milestone' });
        }
    });
    jwplayer(pDivId).onError(function (event) {
        jwplayer(pDivId).load({ file: pURL });
        jwplayer(pDivId).play();
    });
}

function getMinutesSeconds(videoDuration) {
    var videoLengthTotal = "0";
    try {
        videoLengthTotal = videoDuration / 60;
        var videoTimes = videoLengthTotal.toString().split(".");
        var videoLengthMinutes = videoTimes[0];
        var videoLengthSeconds = "." + videoTimes[1];
        videoLengthSeconds = Math.round(60 * videoLengthSeconds);
        videoLengthTotal = videoLengthMinutes + "m" + videoLengthSeconds + "s";
    } catch (err) {
    }
    return videoLengthTotal;
}

/*! http://responsiveslides.com v1.54 by @viljamis */
(function (c, I, B) {
    c.fn.responsiveSlides = function (l) {
        var a = c.extend({ auto: !0, speed: 500, timeout: 4E3, pager: !1, nav: !1, random: !1, pause: !1, pauseControls: !0, prevText: "Previous", nextText: "Next", maxwidth: "", navContainer: "", manualControls: "", namespace: "", before: c.noop, after: c.noop }, l); return this.each(function () {
            B++; var f = c(this), s, r, t, m, p, q, n = 0, e = f.children(), C = e.size(), h = parseFloat(a.speed), D = parseFloat(a.timeout), u = parseFloat(a.maxwidth), g = a.namespace, d = g + B, E = g + "_nav " + d + "_nav", v = g + "_here", j = d + "_on",
                w = d + "_s", k = c("<ul class='" + g + "_tabs " + d + "_tabs' />"), x = { "float": "left", position: "relative", opacity: 1, zIndex: 2 }, y = { "float": "none", position: "absolute", opacity: 0, zIndex: 1 }, F = function () { var b = (document.body || document.documentElement).style, a = "transition"; if ("string" === typeof b[a]) return !0; s = ["Moz", "Webkit", "Khtml", "O", "ms"]; var a = a.charAt(0).toUpperCase() + a.substr(1), c; for (c = 0; c < s.length; c++)if ("string" === typeof b[s[c] + a]) return !0; return !1 }(), z = function (b) {
                    a.before(b); F ? (e.removeClass(j).css(y).eq(b).addClass(j).css(x),
                        n = b, setTimeout(function () { a.after(b) }, h)) : e.stop().fadeOut(h, function () { c(this).removeClass(j).css(y).css("opacity", 1) }).eq(b).fadeIn(h, function () { c(this).addClass(j).css(x); a.after(b); n = b })
                }; a.random && (e.sort(function () { return Math.round(Math.random()) - 0.5 }), f.empty().append(e)); e.each(function (a) { this.id = w + a }); f.addClass(g + " " + d); l && l.maxwidth && f.css("max-width", u); e.hide().css(y).eq(0).addClass(j).css(x).show(); F && e.show().css({
                    "-webkit-transition": "opacity " + h + "ms ease-in-out", "-moz-transition": "opacity " +
                        h + "ms ease-in-out", "-o-transition": "opacity " + h + "ms ease-in-out", transition: "opacity " + h + "ms ease-in-out"
                }); if (1 < e.size()) {
                    if (D < h + 100) return; if (a.pager && !a.manualControls) { var A = []; e.each(function (a) { a += 1; A += "<li><a href='#' class='tooltip" + a + " " + w + a + "'></a></li>" }); k.append(A); l.navContainer ? c(a.navContainer).append(k) : f.after(k) } a.manualControls && (k = c(a.manualControls), k.addClass(g + "_tabs " + d + "_tabs")); (a.pager || a.manualControls) && k.find("li").each(function (a) { c(this).addClass(w + (a + 1)) }); if (a.pager || a.manualControls) q =
                        k.find("a"), r = function (a) { q.closest("li").removeClass(v).eq(a).addClass(v) }; a.auto && (t = function () { p = setInterval(function () { e.stop(!0, !0); var b = n + 1 < C ? n + 1 : 0; (a.pager || a.manualControls) && r(b); z(b) }, D) }, t()); m = function () { a.auto && (clearInterval(p), t()) }; a.pause && f.hover(function () { clearInterval(p) }, function () { m() }); if (a.pager || a.manualControls) q.bind("click", function (b) { b.preventDefault(); a.pauseControls || m(); b = q.index(this); n === b || c("." + j).queue("fx").length || (r(b), z(b)) }).eq(0).closest("li").addClass(v),
                            a.pauseControls && q.hover(function () { clearInterval(p) }, function () { m() }); if (a.nav) {
                                g = "<a href='#' class='" + E + " prev'>" + a.prevText + "</a><a href='#' class='" + E + " next'>" + a.nextText + "</a>"; l.navContainer ? c(a.navContainer).append(g) : f.after(g); var d = c("." + d + "_nav"), G = d.filter(".prev"); d.bind("click", function (b) { b.preventDefault(); b = c("." + j); if (!b.queue("fx").length) { var d = e.index(b); b = d - 1; d = d + 1 < C ? n + 1 : 0; z(c(this)[0] === G[0] ? b : d); if (a.pager || a.manualControls) r(c(this)[0] === G[0] ? b : d); a.pauseControls || m() } });
                                a.pauseControls && d.hover(function () { clearInterval(p) }, function () { m() })
                            }
                } if ("undefined" === typeof document.body.style.maxWidth && l.maxwidth) { var H = function () { f.css("width", "100%"); f.width() > u && f.css("width", u) }; H(); c(I).bind("resize", function () { H() }) }
        })
    }
})(jQuery, this, 0);








if ($jq10('#service-container').length) {
    var iot = iot || {};
    iot.selector = {
        servTitle: '.serv-title',
        serviceList: '.service-list',
        header: '#header',
        list: '.list'
    };
    iot.classes = {
        active: 'active'
    };
    $jq10(document).ready(function () {
        $jq10(iot.selector.servTitle).click(function (e) {
            e.preventDefault();
            var $this = $jq10(this),
                $parentLi = $jq10(this).parents('li');

            $this.toggleClass(iot.classes.active);
            if (!$parentLi.hasClass(iot.classes.active)) {
                $jq10(this).parents(iot.selector.list).children('li').removeClass(iot.classes.active);
            }
            $parentLi.toggleClass(iot.classes.active);
            /** scroll up to plus sign */
            if ($parentLi.length)
                window.scrollTo(0, $parentLi.offset().top - $jq10(iot.selector.header).innerHeight() - 10);
        });
    });
}



!function (t) { function e() { var e, i, n = { height: a.innerHeight, width: a.innerWidth }; return n.height || (e = r.compatMode, (e || !t.support.boxModel) && (i = "CSS1Compat" === e ? f : r.body, n = { height: i.clientHeight, width: i.clientWidth })), n } function i() { return { top: a.pageYOffset || f.scrollTop || r.body.scrollTop, left: a.pageXOffset || f.scrollLeft || r.body.scrollLeft } } function n() { var n, l = t(), r = 0; if (t.each(d, function (t, e) { var i = e.data.selector, n = e.$element; l = l.add(i ? n.find(i) : n) }), n = l.length) for (o = o || e(), h = h || i(); n > r; r++)if (t.contains(f, l[r])) { var a, c, p, s = t(l[r]), u = { height: s.height(), width: s.width() }, g = s.offset(), v = s.data("inview"); if (!h || !o) return; g.top + u.height > h.top && g.top < h.top + o.height && g.left + u.width > h.left && g.left < h.left + o.width ? (a = h.left > g.left ? "right" : h.left + o.width < g.left + u.width ? "left" : "both", c = h.top > g.top ? "bottom" : h.top + o.height < g.top + u.height ? "top" : "both", p = a + "-" + c, v && v === p || s.data("inview", p).trigger("inview", [!0, a, c])) : v && s.data("inview", !1).trigger("inview", [!1]) } } var o, h, l, d = {}, r = document, a = window, f = r.documentElement, c = t.expando; t.event.special.inview = { add: function (e) { d[e.guid + "-" + this[c]] = { data: e, $element: t(this) }, l || t.isEmptyObject(d) || (l = setInterval(n, 250)) }, remove: function (e) { try { delete d[e.guid + "-" + this[c]] } catch (i) { } t.isEmptyObject(d) && (clearInterval(l), l = null) } }, t(a).bind("scroll resize scrollstop", function () { o = h = null }), !f.addEventListener && f.attachEvent && f.attachEvent("onfocusin", function () { h = null }) }(jQuery);


/*****************  Below Code Moved to videoBox.js ****************/
/*****************
'use strict';

(function($) {  
    $.fn.videoBox = function(options) {
        var mediaTitle;
        $(this).click(function(e) {
            var id = $(this).attr("id");
            var href = $(this).attr("href");
            mediaTitle = $(this).attr("data-mediatitle");
            var mediaId   = $(this).attr("data-mediaid");
            var pageid    = $(this).attr("data-pageid");
            var videosource = $(this).attr("data-video-source");
            var mediaImage = $(this).attr("data-image");
            var mediaPath = $(this).attr("data-path");
            var mediaccFile = $(this).attr("data-cc-file");
            var wtattr = $(this).attr("data-wtattr");
            var showMedia = "showVideo";
            href = (videosource == "youtube")? "media.mp4" : href;
            e.preventDefault();
            if ($.cookie('idCookie')) { $.cookie('idCookie', $.cookie('idCookie') + "," + id);
            } else { $.cookie('idCookie', id); }
            if ($('#vidlightbox').length > 0) { 
                $('#vidcontent').html('<div id="mediaContent" class="mediaWrap" style="position:relative;"></div><div class="vid-meta"><p class="vid-title">' + mediaTitle + '</div><a class="vid-close">Close</a>');               
                $('#vidlightbox').show();
            } else{
                var lightbox = 
                '<div id="vidlightbox">' +
                    '<div id="vidcontent">' + 
                        '<div id="mediaContent" class="mediaWrap" style="position:relative;"></div>' +
                        '<div class="vid-meta"><p class="vid-title">' + mediaTitle + '</div><a class="vid-close">Close</a>' +
                    '</div>' +  
                '</div>';
                $('body').append(lightbox);
                if((window.innerWidth || document.documentElement.clientWidth) < 768) {
                    $('#vidcontent').css('width','320px');
                    // vid_url param  width = mobile view
                }
                // code for changing the title wrapping for firefox and IE
                // code for mp3 player change the margin-top
            }
            $("#overlayfid").attr("src", "/mediaplayer_overlay.jsp?" + "&overlay=true&tracking=true&mediaid=" + mediaId + "&mediatitle=" + escape(mediaTitle) 
                                            + "&pageid=" + pageid + "&wtattr=" + wtattr + "&" + showMedia + "=true");
            var vid_url = "/mediaplayer_overlay.jsp?file=" + href + "&mediaid=" + mediaId + "&overlay=";
            vid_url += (videosource == "youtube") ? ("iotyt" + "&mediaImage=" + mediaImage + "&mediaPath=" + mediaPath + "&mediaccFile=" + mediaccFile) : "iot";
            $('.mediaWrap').load(vid_url);

            $(document).on('click', '.vid-close', function () {
                if (jwplayer('jwPlayer') != null) {
                    try {
                    jwplayer('jwPlayer').remove();
                    } catch(err) {}
                }
                $('#vidlightbox').hide();
            });
        });
    };
}(jQuery));


******************/
/*****************  End Code Moved to videoBox.js ****************/

/* ============================= */
// Days Difference Function
/* ============================= */

(function ($) {
    $.fn.daysDiff = function () {
        var countContainer = this;

        $(countContainer).addClass('countContainer');

        $(this).append(' <span class="countcont days-no"></span><span class="countcont"> until <span class="shut-date orng"></span></span>');

        var enddate = $(this).data("enddate");


        // Find Today's Date
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

        var start = d.getFullYear() + '/ ' +
            (('' + month).length < 2 ? '0' : '') + month + '/ ' +
            (('' + day).length < 2 ? '0' : '') + day;


        var start = new Date(start);
        var end = new Date(enddate);

        var diff = end - start;
        var daysDiff = Math.floor((end - start + 1) / (1000 * 60 * 60 * 24));


        if (daysDiff > 1) {
            $('.days-no').html('<span class="days">' + daysDiff + '</span>' + ' <small>DAYS</small>');
        } else {
            $('.days-no').html('<span class="days">' + daysDiff + '</span>' + ' <small>DAY</small>');
        }

        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var dd = end.getDate();
        var mm = monthNames[end.getMonth()];
        var yy = end.getFullYear();

        var shutDate = (mm + " " + dd + ", " + yy);

        $('.shut-date').html(shutDate);

    }

}(jQuery));

$jq10(document).ready(function () {
    $jq10('.daily-counter').daysDiff();
});

/**
 * goto component
 * @namespace att.entbus.goto 
 * @type {object}
 * @required jQuery library
 */
var att = att || {};
att.entbus = att.entbus || {};

att.entbus.goto = (function ($) {

    var selector = {
        gotoComp: '#header .goto-container',
        gotoOptions: '#header .goto-options',
        globalMenuItems: '#leftSegMenu li a.segMenuItem',
        globalMenuPopContainer: '.gm-pop-container',
        primaryWrapper: '#primary-wrapper',
        socialWrapper: '#social-wrapper'
    };

    $(function () {
        att.entbus.goto.bindEvents();
    });

    return {

        bindEvents: function () {
            /** goto component handler */
            $(selector.gotoComp).bind('mouseover mouseout', function () {
                $(selector.gotoOptions).toggle();
                $(this).toggleClass('active');
            });
            /** global menu popup handler */
            $(selector.globalMenuItems).bind('mouseover mouseout', function () {
                var $this = $(this);
                $(selector.globalMenuPopContainer).hide();
                $(selector.globalMenuItems).removeClass('active');
                $this.parent('li').find(selector.globalMenuPopContainer).show();
                $this.addClass('active');
            });
            /** To close global menu popup */
            $(selector.primaryWrapper + ', ' + selector.socialWrapper).bind('mouseover', function () {
                $(selector.globalMenuPopContainer).hide();
                $(selector.globalMenuItems).removeClass('active');
            });

        }
    };

})(jQuery.noConflict());


/** Widgets **/

var att = att || {};
att.entbus = att.entbus || {};
att.entbus.widgets = (function ($) {

    var selector = {
        widgetButtonsShowHide: "#widget-buttons-sh",
        widgetsButtonsHolder: ".widget-button-holder",
        widgetsButton: ".widget-button-holder .widget-button"
    },
        isMobile;


    //set widgets according to screen size
    function resetWidgets() {
        if (isMobile) {
            var buttonWidth = $(selector.widgetsButton).width(),
                buttonMarginLeft = 4,
                numberOfButtons = $(selector.widgetsButton).length;
            //set with of buttons holder
            var widthOfHolder = ((buttonWidth + buttonMarginLeft) * numberOfButtons) + 4;//four is to extend the white bg till the show hide button

            $(selector.widgetButtonsShowHide).css('right', '0px');
            $(selector.widgetButtonsShowHide).removeClass('close open').addClass('open');
            $(selector.widgetsButtonsHolder).css('right', '-250px').css('width', widthOfHolder + 'px');
        }
        else {
            $(selector.widgetButtonsShowHide).css('right', '-250px');
            $(selector.widgetsButtonsHolder).css('right', '0px').css('opacity', '1').css('width', '70px');
        }
    }

    function checkScreenSize() {
        isMobile = window.innerWidth < 768;
        resetWidgets();
    }

    checkScreenSize();

    $(window).bind('resize', checkScreenSize);


    $(function () {
        att.entbus.widgets.events();
    });


    return {
        events: function () {
            $(selector.widgetButtonsShowHide).bind('click', function (e) {
                e.preventDefault();
                if ($(this).hasClass('close')) {
                    $(this).removeClass('close').addClass('open');
                    $(selector.widgetsButtonsHolder).animate({ right: '9px', opacity: '0' }, 200, function () { $(selector.widgetsButtonsHolder).css({ right: '-250px', opacity: '0' }); });
                }
                else if ($(this).hasClass('open')) {
                    $(this).removeClass('open').addClass('close');
                    $(selector.widgetsButtonsHolder).css({ right: '9px', opacity: '0' });
                    $(selector.widgetsButtonsHolder).animate({ right: "66px", opacity: "1" }, 200);
                }
            });
        }
    }

})(jQuery.noConflict());
